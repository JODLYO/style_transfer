import cv2
import os
from pathlib import Path
import sys
from os.path import isfile, join
import style_transfer
import moviepy.video.io.ImageSequenceClip
import imageio

def turn_vid_into_pics(vid_path, pics_path):
    vid_capture = cv2.VideoCapture('clever_doggo.mp4')
    success, image = vid_capture.read()
    no_frames = 0
    
    while success:
      cv2.imwrite("mp4_imgs/frame%d.jpg" % no_frames, image)     # save frame as JPEG file
      success, image = vid_capture.read()
      no_frames += 1
      
    return no_frames

def pics2vid(style_img_folder, video_name, gif_name, length_of_vid):
    frames = []
    for ii in range(0, total_frames + 10, style_frame_jump):
        frames.append(cv2.imread(os.path.join(style_img_folder, 'frame' + str(ii) + '.jpg'))[:, :, ::-1])
    
    fps = len(frames) / length_of_vid
    
    clip = moviepy.video.io.ImageSequenceClip.ImageSequenceClip(frames, fps=fps)
    clip.write_videofile(video_name)
    imageio.mimsave(gif_name, frames)
    return None
  
if __name__ == '__main__':    
    turn_vid_into_pics('clever_doggo.mp4', 'mp4_imgs')
    
    style_img_folder = 'new_mp4_imgs'
    total_frames = 150
    style_frame_jump = 10
    length_of_vid = 5
    
    style_weight = 1
    content_weight = 1
    output_size = 600
    no_iter = 20
    style_img_path = 'style.jpg'
    style_layer_weights = [0.2 for _ in range(5)]
    style_layers = ['block1_conv1', 'block2_conv1', 'block3_conv1', 'block4_conv1', 'block5_conv1']
    content_layers = ['block5_conv2']
    
    
    for ii in range(0, total_frames + 10, style_frame_jump):
        content_img_path = 'mp4_imgs/frame' + str(ii) + '.jpg'
        output_path = os.path.join(style_img_folder, 'frame' + str(ii) + '.jpg')
        
        style_transfer.almost_main(output_size, style_img_path, content_img_path, style_layer_weights,
                                   style_weight, content_weight,
                        style_layers, content_layers, no_iter, generated_img_strat = 'content',
                        output_path=output_path)
    
    pics2vid(style_img_folder, 'my_video.mp4', 5)
