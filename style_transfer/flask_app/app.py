from flask import Flask, render_template, request          # import flask
import os
import style_transfer
from matplotlib import pyplot as plt
import cv2

IMAGE_FOLDER = os.path.join('static', 'images')
app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['UPLOAD_FOLDER'] = IMAGE_FOLDER

@app.route("/")
def main():
    content_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'content.jpg')
    style_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'style.jpg')
    return render_template('index.html', content_img = content_filename, style_img = style_filename)

@app.route("/calculate", methods=['POST'])
def calculate():
    style_weight = float(request.form['style_weight'])
    content_weight = float(request.form['content_weight'])
    output_size = int(request.form['output_size'])
    no_iter = int(request.form['no_iter'])
    style_img_path = os.path.join(IMAGE_FOLDER, 'style.jpg')
    content_img_path = os.path.join(IMAGE_FOLDER, 'content.jpg')
    style_layer_weights = [float(request.form['style_1']),
                           float(request.form['style_2']),
                           float(request.form['style_3']),
                           float(request.form['style_4']),
                           float(request.form['style_5'])]
    style_layers = ['block1_conv1', 'block2_conv1', 'block3_conv1', 'block4_conv1', 'block5_conv1']
    content_layers = ['block5_conv2']
    start_img = request.form['start_img']

    print('style is {} content is {} output is {} no iter is {} style weights are {} start_img is {}'.\
          format(style_weight, content_weight, output_size, no_iter, style_layer_weights, start_img))
        
    style_transfer.generate_img(output_size, style_img_path, content_img_path,
            style_layer_weights, style_weight, content_weight, style_layers,
            content_layers, no_iter, generated_img_strat = start_img,
            generated_img_strat_path = os.path.join(app.config['UPLOAD_FOLDER'], 'start.jpg'))
    
    generated_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'generated_img.jpg')
    content_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'content.jpg')
    style_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'style.jpg')
    return render_template('index.html', generated_image=generated_filename,
                           content_img = content_filename, style_img = style_filename)

@app.route("/upload_image", methods=["GET", "POST"])
def upload_image():
    if request.method == "POST":
        if request.files:
            content_image = request.files["up_content_img"]
            style_image = request.files["up_style_img"]
            start_img = request.files["start_img"]
            content_image.save(os.path.join(app.config["UPLOAD_FOLDER"], 'content.jpg'))
            style_image.save(os.path.join(app.config["UPLOAD_FOLDER"], 'style.jpg'))
            start_img.save(os.path.join(app.config["UPLOAD_FOLDER"], 'start.jpg'))
            print("Image saved")
    generated_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'generated_img.jpg')
    content_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'content.jpg')
    style_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'style.jpg')
    return render_template('index.html', generated_image=generated_filename,
                           content_img = content_filename, style_img = style_filename)
# No caching at all for API endpoints.
@app.after_request
def add_header(response):
    # response.cache_control.no_store = True
    response.headers['Cache-Control'] =\
    'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = '-1'
    return response

if __name__ == "__main__":
    app.run(debug=True)