from typing import Dict, Any, Union
import cv2
import numpy as np
import tensorflow as tf
from tqdm import tqdm
from matplotlib import pyplot as plt

def get_and_resize_img(img_path, max_size): #max res
    img = cv2.imread(img_path)
    scale = max_size / np.max(img.shape)
    new_size = np.array(img.shape[:2][::-1]) * scale
    new_size = tuple(new_size.astype(int))
    img = cv2.resize(img, new_size)
    return img

def get_img(img_path):
    img = cv2.imread(img_path)
    return img

def gram_matrix_tf(input_tensor):
  result = tf.linalg.einsum('bijc,bijd->bcd', input_tensor, input_tensor)
  input_shape = tf.shape(input_tensor)
  num_locations = tf.cast(input_shape[1]*input_shape[2], tf.float32)
  return result/(num_locations)

def get_style_cost_tf(a_S, a_G):
    gram_S = gram_matrix_tf(a_S)
    gram_G = gram_matrix_tf(a_G)
    gram_S = tf.reshape(gram_S, [gram_S.shape[1] * gram_S.shape[2]])
    gram_G = tf.reshape(gram_G, [gram_G.shape[1] * gram_G.shape[2]])
    J = tf.math.reduce_sum(tf.math.square(gram_G - gram_S))
    return J

def get_total_style_cost_tf(style_tensors, generated_tensors):
    no_layers = len(style_tensors)
    J = 0
    for ii in range(no_layers):
        J += get_style_cost_tf(style_tensors[ii], generated_tensors[ii])
    return J

def get_content_cost_tf(a_C, a_G):
    n_H, n_W, n_C = a_C.shape
    a_C = tf.reshape(a_C, (n_H * n_W, n_C))
    a_G = tf.reshape(a_G, (n_H * n_W, n_C))
    J = tf.math.reduce_sum(tf.math.square(a_C - a_G))
    return J

def get_total_content_cost_tf(content_tensors, generated_tensors):
    no_layers = len(content_tensors)
    J = 0
    for ii in range(no_layers):
        J += get_content_cost_tf(content_tensors[ii], generated_tensors[ii])
    return J

def get_total_cost_tf(content_tensors, generated_tensors_c, generated_tensors_s, style_tensors, style_const, content_const):
    J = content_const * get_total_content_cost_tf(content_tensors, generated_tensors_c)
    J += style_const * get_total_style_cost_tf(style_tensors, generated_tensors_s)
    return J

#########

def get_style_cost(a_S, a_G):
    gram_S = gram_matrix(a_S)
    gram_G = gram_matrix(a_G)
    gram_S = gram_S.reshape(gram_S.shape[0] ** 2)
    gram_G = gram_G.reshape(gram_G.shape[0] ** 2)
    J = sum(np.power(gram_S - gram_G, 2))
    return J

def get_content_cost(a_C, a_G):
    m, n_H, n_W, n_C = a_G.shape
    a_C = np.array(a_C).reshape((n_H * n_W * n_C))
    a_G = np.array(a_G).reshape((n_H * n_W * n_C))
    J: float = sum(np.power(a_C - a_G, 2))
    assert isinstance(J, float)
    return J

def gram_matrix(output_tensor):
    output_tensor_shape = output_tensor.shape
    num_rows = output_tensor_shape[1] * output_tensor_shape[2]
    matrix = np.array(output_tensor).reshape((num_rows, output_tensor_shape[3]))
    return matrix.T.dot(matrix) / num_rows



###########

def get_style_tensors(style_layers, vgg_model, style_img_tensor):
    style_outputs = get_vgg_outputs_from_layers(style_layers, vgg_model)
    style_model = tf.keras.Model([vgg_model.input], style_outputs)
    style_tensors = style_model(style_img_tensor)
    return style_tensors

def get_content_tensors(content_layers, vgg_model, content_img_tensor):
    content_outputs = get_vgg_outputs_from_layers(content_layers, vgg_model)
    content_model = tf.keras.Model([vgg_model.input], content_outputs)
    content_tensors = content_model(content_img_tensor)
    return content_tensors

def get_vgg_19_model():
    vgg_model = tf.keras.applications.VGG19(include_top=False, weights='imagenet')
    vgg_model.trainable = False
    return vgg_model

def get_vgg_layer_names_and_outputs(vgg_model):
    layer_names = [vgg_model.layers[i].name for i in range(len(vgg_model.layers))]
    outputs = [vgg_model.get_layer(name).output for name in layer_names]
    return layer_names, outputs

def get_vgg_outputs_from_layers(layer_names, vgg_model):
    outputs = [vgg_model.get_layer(name).output for name in layer_names]
    return outputs

def clip_0_1(image):
  return tf.clip_by_value(image, clip_value_min=0.0, clip_value_max=1.0)

def train_step(generated_img_tensor, output_tensor_dict, hyperparameter_dict, opt, vgg_model, style_layers, content_layers):
    with tf.GradientTape() as tape:
        style_tensors_s = output_tensor_dict['style_tensors_s']
        content_tensor_c = output_tensor_dict['content_tensor_c']
        style_weight = hyperparameter_dict['style_weight']
        content_weight = hyperparameter_dict['content_weight']

        style_tensors_g = get_style_tensors(style_layers, vgg_model, generated_img_tensor)
        content_tensor_g = get_content_tensors(content_layers, vgg_model, generated_img_tensor)

        loss = get_total_cost_tf(content_tensor_c, content_tensor_g, style_tensors_g,
                        style_tensors_s, style_weight, content_weight)
    grad = tape.gradient(loss, generated_img_tensor)
    opt.apply_gradients(zip([grad], [generated_img_tensor]))
    generated_img_tensor.assign(clip_0_1(generated_img_tensor))
    return generated_img_tensor

def tensor_to_image(tensor):
  tensor = tensor*255
  img = np.squeeze(np.array(tensor, dtype=np.uint8))
  return img

def create_hyperparameter_dict(style_layer_weights, style_layers, style_weight, content_weight):
    hyperparameter_dict = {}
    hyperparameter_dict['style_layer_weights'] = {}
    for ii, layer in enumerate(style_layers):
        hyperparameter_dict['style_layer_weights'][layer] = style_layer_weights[ii]
    hyperparameter_dict['style_weight'] = style_weight
    hyperparameter_dict['content_weight'] = content_weight
    return hyperparameter_dict

def get_output_tensor_dict(style_layers, content_layers, vgg_model, style_img_tensor, generated_img_tensor, content_img_tensor):
    output_tensor_dict = {}
    output_tensor_dict['style_tensors_s'] = get_style_tensors(style_layers, vgg_model, style_img_tensor)
    output_tensor_dict['style_tensors_g'] = get_style_tensors(style_layers, vgg_model, generated_img_tensor)
    output_tensor_dict['content_tensor_c'] = get_content_tensors(content_layers, vgg_model, content_img_tensor)
    output_tensor_dict['content_tensor_g'] = get_content_tensors(content_layers, vgg_model, generated_img_tensor)
    return output_tensor_dict

def get_generated_img(content_img, generated_img_strat, generated_img_strat_path, max_output_img_len_width):
    if generated_img_strat == 'content':
        generated_img = content_img.copy()
    elif generated_img_strat == 'random':
        generated_img = (np.random.random_sample(content_img.shape) * 255).astype(int)
    elif generated_img_strat == 'path':
        generated_img = get_and_resize_img(generated_img_strat_path, max_output_img_len_width)
    return generated_img

def generate_img(max_output_img_len_width, style_img_path, content_img_path,
                style_layer_weights, style_weight, content_weight, style_layers,
                content_layers, number_epochs, generated_img_strat = 'content',
                generated_img_strat_path = None, output_path = 'static/images/generated_img.jpg'):
    style_img = get_img(style_img_path)
    content_img = get_and_resize_img(content_img_path, max_output_img_len_width)
    generated_img = get_generated_img(content_img, generated_img_strat, generated_img_strat_path, max_output_img_len_width)
    #generated_img = cv2.resize(generated_img, content_img.shape[:2])
    style_img_tensor = tf.Variable(style_img.reshape((1,) + style_img.shape).astype('float32') / 255)
    content_img_tensor = tf.Variable(content_img.reshape((1,) + content_img.shape).astype('float32') / 255)
    generated_img_tensor = tf.Variable(generated_img.reshape((1,) + content_img.shape).astype('float32') / 255)
    hyperparameter_dict = create_hyperparameter_dict(style_layer_weights, style_layers, style_weight, content_weight)
    vgg_model = get_vgg_19_model()
    output_tensor_dict = get_output_tensor_dict(style_layers, content_layers, vgg_model, style_img_tensor, generated_img_tensor, content_img_tensor)
    opt = tf.optimizers.Adam(learning_rate=0.02, beta_1=0.99, epsilon=1e-1)
    for _ in tqdm(range(number_epochs)):
        generated_img_tensor = train_step(generated_img_tensor, output_tensor_dict, hyperparameter_dict, opt, vgg_model, style_layers, content_layers)
        output = tensor_to_image(generated_img_tensor)
        cv2.imwrite(output_path, output)
    return generated_img


if __name__ == '__main__':
    max_output_img_len_width = 300
    style_img_path = 'static/images/style.jpg'
    content_img_path = 'static/images/content.jpg'
    style_layer_weights = [0.2 for i in range(5)]
    style_weight = 10
    content_weight = 1
    style_layers = ['block1_conv1', 'block2_conv1', 'block3_conv1', 'block4_conv1', 'block5_conv1']
    content_layers = ['block5_conv2']
    number_epochs = 20
    output_path = 'static/images/generated_img.jpg'

    generate_img(max_output_img_len_width, style_img_path, content_img_path,
                style_layer_weights, style_weight, content_weight, style_layers,
                content_layers, number_epochs)








